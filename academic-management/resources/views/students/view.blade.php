@extends('layouts.app', ['activePage' => 'students', 'titlePage' => __('View Student Information')])

@section('content')
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-header-primary">
            <h4 class="card-title ">View Student Information</h4>
            <p class="card-category">View student demographic and enrollment data.</p>
          </div>
          <div class="card-body">
            <h3>{{ $students->first_name }} {{ $students->last_name }}</h3>
            <p>Date of Birth: {{ $students->DOB }}</p>
            <p>Phone: {{ $students->Phone }}</p>
            <p>Email: {{ $students->email }}</p>

            <h3>Enrollments</h3>
            <div class="table-responsive">
                <table class="table">
                  <thead class=" text-primary">
                    <th>Course Number</th>
                    <th>Course Name</th>
                    <th>Grade</th>
                  </thead>
                  <tbody>
                    @foreach ($enrollments as $enrollment)
                    <tr>
                        <td>{{ $enrollment->course_id }}</td>
                        <td>{{ str_replace('m','-', str_replace('p','+', $enrollment->grade)) }}</td>
                        <td><a href="/students/{{ $students->id }}/enrollments/{{ $enrollment->id }}/edit/">Edit</a> | <a href="/students/{{ $students->id }}/enrollments/{{ $enrollment->id }}/delete/">Delete</a> 
                    </tr>
                    @endforeach
                  </tbody>
                </table>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection