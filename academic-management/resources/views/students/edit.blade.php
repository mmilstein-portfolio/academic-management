@extends('layouts.app', ['activePage' => 'students', 'titlePage' => __('Edit Student Information')])

@section('content')
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-header-primary">
            <h4 class="card-title ">Edit Student</h4>
            <p class="card-category">Updates student demographic data.</p>
          </div>
          <div class="card-body">
            <form method="post" action="/students/update/" autocomplete="off" class="form-horizontal">
                @csrf
                @method('put')
                @if (session('status'))
                    <div class="row">
                    <div class="col-sm-12">
                        <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <i class="material-icons">close</i>
                        </button>
                        <span>{{ session('status') }}</span>
                        </div>
                    </div>
                    </div>
                @endif
                <div class="row">
                    <label class="col-sm-2 col-form-label">{{ __('First Name') }}</label>
                    <div class="col-sm-7">
                        <div class="form-group{{ $errors->has('first_name') ? ' has-danger' : '' }}">
                            <input class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="first_name" id="input-first_name" type="text" placeholder="{{ __('First Name') }}" value="{{ $students->first_name }}" required="true" aria-required="true"/>
                            @if ($errors->has('first_name'))
                            <span id="first_name-error" class="error text-danger" for="input-first_name">{{ $errors->first('first_name') }}</span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-2 col-form-label">{{ __('Last Name') }}</label>
                    <div class="col-sm-7">
                        <div class="form-group{{ $errors->has('last_name') ? ' has-danger' : '' }}">
                            <input class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="first_name" id="input-last_name" type="text" placeholder="{{ __('First Name') }}" value="{{ $students->last_name }}" required="true" aria-required="true"/>
                            @if ($errors->has('last_name'))
                            <span id="last_name-error" class="error text-danger" for="input-last_name">{{ $errors->first('first_name') }}</span>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="row">
                    <label class="col-sm-2 col-form-label">{{ __('Email') }}</label>
                    <div class="col-sm-7">
                        <div class="form-group{{ $errors->has('email') ? ' has-danger' : '' }}">
                            <input class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" id="input-email" type="email" placeholder="{{ __('Email') }}" value="{{ $students->email }}" required />
                            @if ($errors->has('email'))
                            <span id="email-error" class="error text-danger" for="input-email">{{ $errors->first('email') }}</span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="card-footer ml-auto mr-auto">
                    <button type="submit" class="btn btn-primary">{{ __('Save') }}</button>
                </div>
            </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection