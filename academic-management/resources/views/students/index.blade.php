@extends('layouts.app', ['activePage' => 'students', 'titlePage' => __('Manage Students')])

@section('content')
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-header-primary">
            <h4 class="card-title ">All Students</h4>
            <p class="card-category">View All Students</p>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table">
                <thead class=" text-primary">
                  <th>
                    ID
                  </th>
                  <th>
                    First Name
                  </th>
                  <th>
                    Last Name
                  </th>
                  <th>
                    DOB
                  </th>
                  <th>
                    Phone
                  </th>
                  <th>
                    Email
                  </th>
                  <th>
                    Actions
                  </th>
                </thead>
                <tbody>
                  @foreach ($students as $student)
                  <tr>
                      <td>{{ $student->id }}</td>
                      <td>{{ $student->first_name }}</td>
                      <td>{{ $student->last_name }}</td>
                      <td>{{ $student->DOB }}</td>
                      <td>{{ $student->Phone }}</td>
                      <td>{{ $student->email }}</td>
                      <td><a href="/students/edit/{{ $student->id }}/">Edit</a> | <a href="/students/view/{{ $student->id }}/">View</a> 
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection