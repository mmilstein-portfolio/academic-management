@extends('layouts.app', ['activePage' => 'courses', 'titlePage' => __('Edit Course Information')])

@section('content')
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-header-primary">
            <h4 class="card-title ">Edit Course Information</h4>
            <p class="card-category">Updates course data.</p>
          </div>
          <div class="card-body">
            <form method="post" action="/courses/update/" autocomplete="off" class="form-horizontal">
                @csrf
                @method('put')
                @if (session('status'))
                    <div class="row">
                    <div class="col-sm-12">
                        <div class="alert alert-success">
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <i class="material-icons">close</i>
                        </button>
                        <span>{{ session('status') }}</span>
                        </div>
                    </div>
                    </div>
                @endif
                <div class="row">
                    <label class="col-sm-2 col-form-label">{{ __('Course Name') }}</label>
                    <div class="col-sm-7">
                        <div class="form-group{{ $errors->has('course_name') ? ' has-danger' : '' }}">
                            <input class="form-control{{ $errors->has('course_name') ? ' is-invalid' : '' }}" name="course_name" id="input-course_name" type="text" placeholder="{{ __('Course Name') }}" value="{{ $courses->course_name }}" required="true" aria-required="true"/>
                            @if ($errors->has('course_name'))
                            <span id="course_name-error" class="error text-danger" for="input-course_name">{{ $errors->first('course_name') }}</span>
                            @endif
                        </div>
                    </div>
                </div>
                <div class="row">
                    <label class="col-sm-2 col-form-label">{{ __('Course Number') }}</label>
                    <div class="col-sm-7">
                        <div class="form-group{{ $errors->has('course_number') ? ' has-danger' : '' }}">
                            <input class="form-control{{ $errors->has('course_number') ? ' is-invalid' : '' }}" name="course_number" id="input-course_number" type="text" placeholder="{{ __('Course Number') }}" value="{{ $courses->course_number }}" required="true" aria-required="true"/>
                            @if ($errors->has('course_number'))
                            <span id="course_number-error" class="error text-danger" for="input-course_number">{{ $errors->first('course_number') }}</span>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="row">
                    <label class="col-sm-2 col-form-label">{{ __('Credits') }}</label>
                    <div class="col-sm-7">
                        <div class="form-group{{ $errors->has('credits') ? ' has-danger' : '' }}">
                            <input class="form-control{{ $errors->has('credits') ? ' is-invalid' : '' }}" name="credits" id="input-credits" type="text" placeholder="{{ __('Credits') }}" value="{{ $courses->credits }}" required="true" aria-required="true"/>
                            @if ($errors->has('credits'))
                            <span id="credits-error" class="error text-danger" for="input-credits">{{ $errors->first('credits') }}</span>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="row">
                    <label class="col-sm-2 col-form-label">{{ __('Instructor') }}</label>
                    <div class="col-sm-7">
                        <div class="form-group{{ $errors->has('instructor') ? ' has-danger' : '' }}">
                            <input class="form-control{{ $errors->has('instructor') ? ' is-invalid' : '' }}" name="instructor" id="input-instructor" type="text" placeholder="{{ __('Instructor') }}" value="{{ $courses->instructor }}" required="true" aria-required="true"/>
                            @if ($errors->has('instructor'))
                            <span id="instructor-error" class="error text-danger" for="input-instructor">{{ $errors->first('instructor') }}</span>
                            @endif
                        </div>
                    </div>
                </div>

                <div class="card-footer ml-auto mr-auto">
                    <button type="submit" class="btn btn-primary">{{ __('Save') }}</button>
                </div>
            </form>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection