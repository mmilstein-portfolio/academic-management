@extends('layouts.app', ['activePage' => 'courses', 'titlePage' => __('Manage Courses')])

@section('content')
<div class="content">
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
          <div class="card-header card-header-primary">
            <h4 class="card-title ">All Courses</h4>
            <p class="card-category">View All Courses</p>
          </div>
          <div class="card-body">
            <div class="table-responsive">
              <table class="table">
                <thead class=" text-primary">
                  <th>
                    Course Number
                  </th>
                  <th>
                    Course Name
                  </th>
                  <th>
                    Credits
                  </th>
                  <th>
                    Instructor
                  </th>
                  <th>
                    Actions
                  </th>
                </thead>
                <tbody>
                  @foreach ($courses as $course)
                  <tr>
                      <td>{{ $course->course_number }}</td>
                      <td>{{ $course->course_name }}</td>
                      <td>{{ $course->credits }}</td>
                      <td>{{ $course->instructor }}</td>
                      <td><a href="/courses/edit/{{ $course->id }}/">Edit</a>
                  </tr>
                  @endforeach
                </tbody>
              </table>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>
@endsection