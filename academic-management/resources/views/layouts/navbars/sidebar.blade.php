<div class="sidebar" data-color="orange" data-background-color="white" data-image="{{ asset('material') }}/img/sidebar-1.jpg">
  <!--
      Tip 1: You can change the color of the sidebar using: data-color="purple | azure | green | orange | danger"

      Tip 2: you can also add an image using data-image tag
  -->
  <div class="logo">
    <a href="#" class="simple-text logo-normal">
      {{ __('Megan Milstein') }}
    </a>
  </div>
  <div class="sidebar-wrapper">
    <ul class="nav">
      <li class="nav-item{{ $activePage == 'dashboard' ? ' active' : '' }}">
        <a class="nav-link" href="{{ route('home') }}">
          <i class="material-icons">dashboard</i>
            <p>{{ __('Dashboard') }}</p>
        </a>
      </li>
      <li class="nav-item {{ ($activePage == 'students' || $activePage == 'students-management') ? ' active' : '' }}">
        <a class="nav-link" href="{{ route('students.main') }}">
          <i class="material-icons">groups</i>
            <p>{{ __('Students') }}</p>
        </a>
      </li>
      <li class="nav-item {{ ($activePage == 'courses' || $activePage == 'courses-management') ? ' active' : '' }}">
        <a class="nav-link" href="{{ route('courses.main') }}">
          <i class="material-icons">book</i>
            <p>{{ __('Courses') }}</p>
        </a>
      </li>
    </ul>
  </div>
</div>
