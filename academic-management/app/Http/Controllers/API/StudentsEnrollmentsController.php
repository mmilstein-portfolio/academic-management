<?php
namespace App\Http\Controllers\API;
   
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Eloquent\Builder;

use App\Http\Controllers\API\BaseController as BaseController;
use App\Http\Resources\Enrollments as EnrollmentsResource;

use App\Models\Enrollments;

class StudentsEnrollmentsController extends BaseController
{

    public function index($id)
    { 
        $enrollments = DB::table('enrollments')
                            ->where('student_id','=', $id)
                            ->join('courses', 'enrollments.course_id', '=', 'courses.id')
                    ->get();

        return $this->handleResponse($enrollments, 'Student enrollments have been retrieved!');
    }

    public function store(Request $request)
    {
        $input = $request->all();
        $enrollments = Enrollments::create($input);

        return $this->handleResponse(new EnrollmentsResource($enrollments), 'Student enrollment created!');
    }
   
    public function show($student_id, $enrollment_id)
    {
        $enrollments = Enrollments::find($enrollment_id);
        if (is_null($enrollments)) {
            return $this->handleError('Enrollment not found!');
        }
        return $this->handleResponse(new EnrollmentsResource($enrollments), 'Enrollment retrieved.');
    }

    public function update(Request $request, $student_id, $enrollment_id)
    {
        $input = $request->all();
        
        $enrollments = Enrollments::find($enrollment_id);
        $enrollments->student_id = $student_id;
        $enrollments->course_id = $input['course_id'];
        $enrollments->grade = $input['grade'];
        $enrollments->term = $input['term'];
        $enrollments->save();
        
        return $this->handleResponse([$enrollment_id, $input], 'Enrollment successfully updated!');
    }
   
    public function destroy($student_id, $enrollment_id)
    {
        Enrollments::destroy($enrollment_id);
        return $this->handleResponse([], 'Enrollment deleted!');
    }
}