<?php
namespace App\Http\Controllers\API;
   
use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Http\Resources\Enrollments as EnrollmentsResource;
use App\Models\Enrollments;

class EnrollmentsController extends BaseController
{

    public function index()
    {
        $enrollments = Enrollments::all();
        return $this->handleResponse(EnrollmentsResource::collection($enrollments), 'Enrollments have been retrieved!');
    }

    
    public function store(Request $request)
    {
        $input = $request->all();
        $enrollments = Enrollments::create($input);
        return $this->handleResponse(new EnrollmentsResource($enrollments), 'Enrollment created!');
    }

   
    public function show($id)
    {
        $enrollments = Enrollments::find($id);
        if (is_null($enrollments)) {
            return $this->handleError('Enrollment not found!');
        }
        return $this->handleResponse(new EnrollmentsResource($enrollments), 'Enrollment retrieved.');
    }
    

    public function update(Request $request, $id)
    {
        $input = $request->all();
        
        $enrollments = Enrollments::find($id);
        $enrollments->student_id = $input['student_id'];
        $enrollments->course_id = $input['course_id'];
        $enrollments->grade = $input['grade'];
        $enrollments->term = $input['term'];
        $enrollments->save();
        
        return $this->handleResponse(new EnrollmentsResource($enrollments), 'Enrollment successfully updated!');
    }
   
    public function destroy($id)
    {
        Enrollments::destroy($id);
        return $this->handleResponse([], 'Enrollment deleted!');
    }
}