<?php
namespace App\Http\Controllers\API;
   
use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Http\Resources\Students as StudentsResource;
use App\Models\Students;

class StudentsController extends BaseController
{

    public function index()
    {
        $students = Students::all();
        return $this->handleResponse(StudentsResource::collection($students), 'Students have been retrieved!');
    }

    
    public function store(Request $request)
    {
        $input = $request->all();
        $students = Students::create($input);
        return $this->handleResponse(new StudentsResource($students), 'Student created!');
    }

   
    public function show($id)
    {
        $students = Students::find($id);
        if (is_null($students)) {
            return $this->handleError('Student not found!');
        }
        return $this->handleResponse(new StudentsResource($students), 'Student retrieved.');
    }

    public function update(Request $request, $id)
    {
        $input = $request->all();
        
        $students = Students::find($id);
        $students->first_name = $input['first_name'];
        $students->last_name = $input['last_name'];
        $students->DOB = $input['DOB'];
        $students->Phone = $input['Phone'];
        $students->email = $input['email'];
        $students->save();
        
        return $this->handleResponse(new StudentsResource($students), 'Student successfully updated!');
    }
   
    public function destroy($id)
    {
        Students::destroy($id);
       
        return $this->handleResponse([], 'Student deleted!');
    }
}