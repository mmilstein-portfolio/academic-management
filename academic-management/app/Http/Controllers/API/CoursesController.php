<?php
namespace App\Http\Controllers\API;
   
use Illuminate\Http\Request;
use App\Http\Controllers\API\BaseController as BaseController;
use App\Http\Resources\Courses as CoursesResource;
use App\Models\Courses;

class CoursesController extends BaseController
{

    public function index()
    {
        $courses = Courses::all();
        return $this->handleResponse(CoursesResource::collection($courses), 'Courses have been retrieved!');
    }

    
    public function store(Request $request)
    {
        $input = $request->all();
        $courses = Courses::create($input);
        return $this->handleResponse(new CoursesResource($courses), 'Course created!');
    }

   
    public function show($id)
    {
        $courses = Courses::find($id);
        if (is_null($courses)) {
            return $this->handleError('Course not found!');
        }
        return $this->handleResponse(new CoursesResource($courses), 'Course retrieved.');
    }
    

    public function update(Request $request, $id)
    {
        $input = $request->all();

        $courses = Courses::find($id);

        $courses->course_number = $input['course_number'];
        $courses->course_name = $input['course_name'];
        $courses->credits = $input['credits'];
        $courses->instructor = $input['instructor'];
        $courses->save();
        
        return $this->handleResponse(new CoursesResource($courses), 'Course successfully updated!');
    }
   
    public function destroy($id)
    {
        Courses::destroy($id);
        return $this->handleResponse([], 'Course deleted!');
    }
}