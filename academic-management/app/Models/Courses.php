<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Courses extends Model
{
    use HasFactory;

    protected $table = 'courses';

    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'course_number',
        'course_name',
        'credits',
        'instructor',
    ];

    // public function instructors()
    // {
    //     return $this->hasMany(Instructors::class, 'course_id');
    // }

}
