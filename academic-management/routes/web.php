<?php

use App\Http\Controllers\API\StudentsController;
use App\Http\Controllers\API\StudentsEnrollmentsController as StudentsEnrollmentsController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;

use App\Models\Students;
use App\Models\Courses;
use App\Models\Enrollments;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

require __DIR__.'/auth.php';

// Auth::routes();

Route::get('/', function () {
    return view('welcome');
});

Route::get('/home', 'App\Http\Controllers\HomeController@index')->name('home')->middleware('auth');

Route::group(['middleware' => 'auth'], function () {
	Route::get('students', function () {
		$students = Students::all();
		return view('students.index', ['students' => $students]);
	})->name('students.main');

	Route::get('students/view/{student}', function ($student) {
		$students = Students::find($student);
		$enrollments = Enrollments::where('student_id','=', $student)->get();

		return view('students.view', ['students' => $students, 'enrollments' => $enrollments]);
	})->name('students.view');

	Route::get('students/edit/{student}', function ($student) {
		$students = Students::find($student);
		return view('students.edit', ['students' => $students]);
	})->name('students.edit');

	Route::post('students/update', ['as' => 'students.update', 'uses' => 'App\Http\Controllers\API\StudentsController@update']);
});

Route::group(['middleware' => 'auth'], function () {
	Route::get('courses', function () {
		$courses = Courses::all();
		return view('courses.index', ['courses' => $courses]);
	})->name('courses.main');

	Route::get('courses/edit/{course}', function ($courses) {
		$courses = Courses::find($courses);
		return view('courses.edit', ['courses' => $courses]);
	})->name('courses.edit');

	Route::delete('courses/delete/{course}', ['as' => 'courses.delete', 'uses' => 'App\Http\Controllers\API\CoursesController@destroy']);

	Route::post('courses/update/{course}', ['as' => 'courses.update', 'uses' => 'App\Http\Controllers\API\CoursesController@update']);
});

Route::group(['middleware' => 'auth'], function () {
	Route::resource('user', 'App\Http\Controllers\UserController', ['except' => ['show']]);
	Route::get('profile', ['as' => 'profile.edit', 'uses' => 'App\Http\Controllers\ProfileController@edit']);
	Route::put('profile', ['as' => 'profile.update', 'uses' => 'App\Http\Controllers\ProfileController@update']);
	Route::put('profile/password', ['as' => 'profile.password', 'uses' => 'App\Http\Controllers\ProfileController@password']);
});

